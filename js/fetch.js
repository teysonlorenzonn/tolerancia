const options = {
  method: 'GET',
  headers: {
  'Content-Type': 'application/json'
  }
};

const getViaCep = async cep => {
  let response = {};
  await fetch(`https://viacep.com.br/ws/${cep}/json/`, options)
    .then(resp => resp.json())
    .then(json => {
      response = {resp: json}
      response['message'] = 'Busca realizada com VIACEP';
      response['error'] = false;
    })
    .catch(e => response = {message: e, error: true});
  return response;
}

const getRepublicaVirtual = async cep => {
  let response = {};
  await fetch(`http://cep.republicavirtual.com.br/web_cep.php?cep=${cep}&formato=json`)
    .then(resp => resp.json())
    .then(json => {
      console.log('j',json)
      response = {resp: json}
      response['message'] = 'Busca realizada com Republica Virtual';
      response['error'] = false;
    })
    .catch(e => response = {message: e, error: true});
  return response;
}