const returns = $('#returns');
const btTeste = $('#teste');
const inputCep = $('#inputCep');

const consult = async cep => {
  let info = await getViaCep(cep);
  if (info.error) {
    info = await getRepublicaVirtual(cep);
  }
 return info;
};

btTeste.click( async e => {
  e.preventDefault();
  let cep = inputCep.val();
  let result = await consult(cep);
  returns.html(JSON.stringify(result));
})